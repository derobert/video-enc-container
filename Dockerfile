FROM debian:latest

ADD usr_local.tar.xz /usr/

RUN true \
	&& apt-get update \
	&& apt-get upgrade -y \
	&& apt-get install -y $(cat /usr/local/needed-pkgs) \
	&& ldconfig \
	&& ( rm -f /var/lib/apt/lists/* || true )
